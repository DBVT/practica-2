package mx.unitec.moviles.practica2


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class LoginActivity : AppCompatActivity() {

    private lateinit var editTextEmail:EditText
    private lateinit var editTextPassword:EditText
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        button = findViewById(R.id.button)

        button.setOnClickListener {

            if(editTextEmail.text.isEmpty())
                editTextEmail.error = getString(R.string.error_text)

            if(editTextPassword.text.isEmpty())
                editTextPassword.error = getString(R.string.error_text)

            else
            Toast.makeText(this,R.string.wellcome_message, Toast.LENGTH_SHORT).show()

        }
    }
}